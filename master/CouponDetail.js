//import liraries
import React, { Component, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native'; 
import * as firebase from "firebase";
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';


// create a component
const CouponDetail = ({couponcode}) => {
    const [ch, setCh] = useState("0");
    const [post, setPost] = useState({});
    if(ch == "0") {
        firebase.database().ref(`posts/` + couponcode).on('value',(snapshot)=>
        {
            if(snapshot.val() == undefined) {
                setPost({})
                setCh("1")
            }
            else {
                setPost(snapshot.val())
                setCh("1")
            }
           })
        }
        const LeftContent = props => <Avatar.Icon {...props} icon="tag" type="font-awesome"/>
    return (
        <View style={styles.container}>
            <Card style={{marginBottom:15}}>
            <Card.Title title={ post.brandpromotionname } subtitle={ post.brandfreedeal } left={LeftContent} />
            <Card.Cover source={{ uri: post.profileImageURL }} />
            <Card.Actions>
              <Button>CODE :</Button>
              <Button>{post.coupon}</Button>
            </Card.Actions>
          </Card>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgrey',
    },
});

//make this component available to the app
export default CouponDetail;
