//import liraries
import React, { Component , useState} from 'react';
import { View, StyleSheet } from 'react-native';
import {ListItem, Overlay, CheckBox, Text } from "react-native-elements";
import {  Paragraph, Dialog, Portal } from 'react-native-paper';
import { Provider } from 'react-native-paper';
import { Button, Icon, ButtonGroup } from "react-native-elements";
import * as firebase from "firebase";



// create a component
const BrandDetail = (brand, i) => {
  const [ch, setCh] = useState("0");
    const [bObj, setBrand] = useState({});
    const [checked, setChecked] = useState(false)
    if(ch == "0") {
        firebase.database().ref(`users/brand/` + brand.brand).on('value',(snapshot)=>
        {
            if(snapshot.val() == undefined) {
                setBrand({})
                setCh("1")
            }
            else {
                setBrand(snapshot.val())
                setCh("1")
            }
           })
        }
    
    const [visible, setVisible] = useState(false);

     const _onPressUpdateBrand = async (checked, brand) => {
       console.log(checked, brand)
       let requestValue = checked
        await firebase
          .database()
          .ref(`users/brand/` + brand.brand)
          .update({
            requestValue, })

            setChecked(!checked);


    }
        

const toggleOverlay = () => {
  setVisible(!visible);

 
    };
  


    return (
      <View>
          
        <View style={styles.container}>
          
          <ListItem
        key={i}
        leftAvatar={{ source: { uri: bObj.profileImageURL } }}
        title={bObj.firstname +` `+ bObj.lastname}
        subtitle={bObj.brandname}
        bottomDivider
        chevron
        onPress={() => {toggleOverlay()}}      />
      

          </View>
          <Overlay isVisible={visible} onBackdropPress={() => {toggleOverlay()}} >
            <View style={{justifyContent:"center", alignItems:"center", textAlign:"center", flex:1, }}>
        
    <Text h4 >ACTIVE STATUS :</Text>
    {bObj.requestValue ? <Text h4 >true</Text> : <Text h4 >false</Text>}

        <CheckBox
        style={{marginTop:50}}
        center
        title='CHANGE STATUS'
  checkedIcon={<Icon
  
    name='check'
    type='font-awesome'
    color='#517fa4'
  />}
  uncheckedIcon={<Icon
    name='times'
    type='font-awesome'
    color='#517fa4'
  />}
  checked={bObj.requestValue}
  onPress={() => {_onPressUpdateBrand(checked,brand)}}
/>

        </View>
      </Overlay>
 
          </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default BrandDetail;
