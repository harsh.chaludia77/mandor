import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBTTXyIOCrRKuIiVlSX56VfmLq8nFdwMh0",
    authDomain: "mandors.firebaseapp.com",
    databaseURL: "https://mandors.firebaseio.com",
    projectId: "mandors",
    storageBucket: "mandors.appspot.com",
    messagingSenderId: "814942265198",
    appId: "1:814942265198:web:b03410dfc397f7faf76b7f",
    measurementId: "G-VVDSDCZWKT"
};

firebase.initializeApp(firebaseConfig);

export default firebase;